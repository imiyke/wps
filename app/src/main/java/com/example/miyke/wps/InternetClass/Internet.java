package com.example.miyke.wps.InternetClass;

import android.app.job.JobScheduler;
import android.widget.Toast;

import com.example.miyke.wps.AddQuestionActivity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by miyke on 08/12/15.
 */
public class Internet
    {
    private static String API_URL = "http://52.18.61.220:53659/wps_webapp/webresources/pack.";

    public static String doRequest(String partUrl, int requestType, JSONObject Jobj)
        {
        switch (requestType)
            {
            case 1:
                return doPostRequest(partUrl, Jobj);
            case 4:
                return doPutRequest(partUrl, Jobj);
            default:
                return doGetRequest(partUrl);
            }

        }

    private static String doPutRequest(String partUrl, JSONObject jobj)
        {
        HttpPut httpPut = new HttpPut(API_URL + partUrl);
        HttpClient client = new DefaultHttpClient();
        StringEntity params = null;//sonObject.toString());
        try
            {
            params = new StringEntity(jobj.toString(), HTTP.UTF_8);
            }
        catch (UnsupportedEncodingException e)
            {
            e.printStackTrace();
            }
        params.setContentType("application/json");
      //  httpPut.addHeader("Content-Type", "application/json;charset=UTF-8");
        int statusCode=400;

       /* StringEntity params = null;
        try
            {
            params = new StringEntity(jobj.toString());
            }
        catch (UnsupportedEncodingException e)
            {
            e.printStackTrace();
            }*/
        StringBuilder sb=new StringBuilder();
        try
            {

            httpPut.setEntity(params);
            HttpResponse response = client.execute(httpPut);
             statusCode= response.getStatusLine().getStatusCode();
//             BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity()
  /**      .getContent()));
            String line = "";
            while ((line = rd.readLine()) != null)  /**utile pour debug
                sb.append(line).append("\n");
            System.out.println("blabla: " + sb.toString());**/
            } catch (IOException e)
            {
            e.printStackTrace();
            e.getCause();

            }
        return Integer.toString(statusCode);

        }

    public static String doGetRequest(String partURL)
        {
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(API_URL + partURL);
        //httpGet.addHeader("Content-Type","application/json;charset=UTF-8");
        StringBuilder sb = new StringBuilder();
        try
            {

            HttpResponse response = client.execute(httpGet);
            if (response.getEntity()!=null)
                {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity()
                        .getContent()));

                String line = "";
                while ((line = rd.readLine()) != null)
                    sb.append(line).append("\n");
                }
            } catch (IOException e)
            {
            e.printStackTrace();
            e.getCause();

            }
        return sb.toString();
        }

    public static String doPostRequest(String partURL, JSONObject jsonObject)
        {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost HttpPost = new HttpPost(API_URL + partURL);
        //  Post.addHeader("http-x-client-id",MainActivity.clienID);
        // Post.addHeader("http-x-client-sig", hash_client_secret2(MainActivity.secret));
      //  HttpPost.addHeader("Content-Type", "application/json");//
        // ("http-x-access-token",HomeActivity.token);


        StringBuilder sb =new StringBuilder();
        try
            {
            String test = jsonObject.toString();
            StringEntity params = new StringEntity(jsonObject.toString(), HTTP.UTF_8);//sonObject.toString());
             params.setContentType("application/json");
             //HttpPost.setHeader("Content-type", "application/json;charset=UTF-8");
            // params.setContentType("application/json;charset=UTF-8");
           // params.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;"+
             //"charset=UTF-8"));

            HttpClient client = new DefaultHttpClient();


            HttpPost.setEntity(params);
            HttpResponse httpResponse = client.execute(HttpPost);
            String resultStatus = httpResponse.getStatusLine().toString();
            //Toast.makeText(AddQuestionActivity.this, resultStatus,Toast.LENGTH_LONG).show();
            //sb=httpResponse.getEntity().getContent().toString();
                if (httpResponse.getEntity()!=null)
                {
                BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity()
                        .getContent()));

                String line = "";
                while ((line = rd.readLine()) != null)
                    sb.append(line).append("\n");
                }
            } catch (IOException e)
            {
            e.printStackTrace();
            }
        System.out.println("API4TEST" + sb.toString());
        return sb.toString();


        }
    }
