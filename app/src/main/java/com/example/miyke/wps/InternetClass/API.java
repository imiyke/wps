package com.example.miyke.wps.InternetClass;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.miyke.wps.Class.ToolKit;
import com.example.miyke.wps.LoginActivity;
import com.example.miyke.wps.MainFlowActivity;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by miyke on 08/12/15.
 */
public class API extends  AsyncTask<Void,Integer,String>
    {
    private String partURL;
    private int    requestType;
    private JSONObject Jobj = new JSONObject();
    private ProgressBar pb ;
    private Context context;
    private LinearLayout linearLayout;

    public interface OnRequestListener
        {
        void finish(String result);

        }

    private OnRequestListener        callback;
    private ArrayList<NameValuePair> args;
   // private ProgressBar pb;

    public API(ProgressBar progressBar,LinearLayout ll,String partURL, JSONObject jobj, int RequestType, OnRequestListener callback)
        {

        if (RequestType == 1 || RequestType == 4)//POST request
            this.Jobj = jobj;
        this.callback = callback;
        this.requestType = RequestType;
        this.partURL = partURL;
      //  this.context=ctx;
        linearLayout=ll;
        pb=progressBar;
        this.execute();
        }
    /**TODO:Voir ce code
    LoadMusicInBackground lmib = new LoadMusicInBackground();

if(lmib.getStatus() == AsyncTask.Status.PENDING){
    // My AsyncTask has not started yet
}

if(lmib.getStatus() == AsyncTask.Status.RUNNING){
    // My AsyncTask is currently doing work in doInBackground()
}

if(lmib.getStatus() == AsyncTask.Status.FINISHED){
    // My AsyncTask is done and onPostExecute was called
}
     **/
    @Override
    protected void onPreExecute()
        {
        LoginActivity.threadList.add(1);

        if (LoginActivity.threadList.size()==1 && pb!=null)

            {
            pb.setVisibility(View.VISIBLE);
            //   pb=ToolKit.setProgressBar(context,linearLayout,pb);
            }
        }


    @Override
    protected String doInBackground(Void... params) {
    return Internet.doRequest(partURL,requestType,Jobj);
    }

   @Override
    protected void onPostExecute(String s)
        {
        if (partURL.contains("wpsquesttable") && this.requestType==3)
            {

            //MainFlowActivity.setListView();
            }
        LoginActivity.threadList.remove(LoginActivity.threadList.size() - 1);

        if ( LoginActivity.threadList.size()==0 && pb!=null)
        ToolKit.stopProgressBar(pb);

        callback.finish(s);
        }
    }
