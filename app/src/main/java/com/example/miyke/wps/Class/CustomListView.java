package com.example.miyke.wps.Class;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.miyke.wps.FavorisActivity;
import com.example.miyke.wps.InternetClass.API;
import com.example.miyke.wps.InternetClass.Internet;
import com.example.miyke.wps.LoginActivity;
import com.example.miyke.wps.MainFlowActivity;
import com.example.miyke.wps.R;
import com.example.miyke.wps.adminActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Text;

import java.io.WriteAbortedException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by miyke on 18/10/15.
 */
public class CustomListView
    {
    Button  buttonMore;
    Context ctx;
    int checked = 0;
    private boolean isChecked = false;
    private ArrayList<Integer> IDquestRepondues;
    private boolean            isFav;

    public CustomListView(final ArrayList<Question> questionList, final LinearLayout ll, Context
            context)
        {
        this.ctx = context;
        isFav = true;
        this.IDquestRepondues = LoginActivity.user.sortReponse()[0];
        int activityNumber=getIntFromActivity(context);
        for (int i = 0; i < questionList.size(); i++)
            {
            final RelativeLayout rl = new RelativeLayout(context);
            initRL(rl, questionList, i, ll, activityNumber);


            //textView.setPadding(10, 25, 0, 0);
            /**param for answer tV**/
            //     .LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            TextView tvTotal = setTextView(Integer.toString(questionList.get(i).totAns));
            final RelativeLayout.LayoutParams paramTot = new RelativeLayout.LayoutParams(ViewGroup
                    .LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            paramTot.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, rl.getId());
            paramTot.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, rl.getId());
            paramTot.setMargins(0, 0, 50, 40);
            rl.addView(tvTotal, paramTot);


            if (ctx != adminActivity.adminContext)
                {

                initParamImageNonAdmin(questionList,i,ll,rl);

                }




            LinearLayout.LayoutParams LP = new LinearLayout.LayoutParams(RelativeLayout
                    .LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            LP.setMargins(0, 0, 0, 32);
            rl.setLayoutParams(LP);
            ll.addView(rl);

            }
        }
    public void setArrow(final RelativeLayout relativeLayout, final ArrayList<Question> questionList, final int pos)
        {
         RelativeLayout.LayoutParams pa = new RelativeLayout.LayoutParams(150,150);

            // pa.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, relativeLayout.getId());
            pa.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, relativeLayout.getId());
            pa.addRule(RelativeLayout.ALIGN_PARENT_LEFT, relativeLayout.getId());
            pa.setMargins(215, 0, 180, 55);

        final Button buttArrow=new Button(ctx);
        buttArrow.setBackgroundResource(R.drawable.swap);//deux boutons, swap enable
        final Button buttonArrow2=new Button(ctx);
        buttonArrow2.setBackgroundResource(R.drawable.swap);
        buttonArrow2.setEnabled(false);
        buttArrow.setOnClickListener(new View.OnClickListener()
        {
        @Override
        public void onClick(View v)
            {
            buttArrow.setVisibility(View.GONE);
            buttonArrow2.setVisibility(View.VISIBLE);
            displayResult(relativeLayout, questionList, pos);
            buttonArrow2.setEnabled(true);

            }
        });
        buttonArrow2.setOnClickListener(new View.OnClickListener()
        {
        @Override
        public void onClick(View v)
            {
            buttonArrow2.setVisibility(View.GONE);
            buttArrow.setVisibility(View.VISIBLE);
            displayAnswer(relativeLayout, questionList, pos);

            }
        });

        relativeLayout.addView(buttArrow,pa);
        relativeLayout.addView(buttonArrow2,pa);
        }
    public void initRL(final RelativeLayout relativeLayout, final ArrayList<Question> questionList,int position,
                       final LinearLayout linearLayout, int activityNum)
        {
        setRLBackColor(activityNum,relativeLayout);
        RelativeLayout.LayoutParams RLP = new RelativeLayout.LayoutParams(ViewGroup
                    .LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            RLP.setMargins(60, 40, 0, 200+questionList.get(position).getQuestion().length()/20);
        initQuestionTextView(questionList, position, relativeLayout, RLP);
         relativeLayout.setLayoutParams(RLP);
        final int pos = position;
            relativeLayout.setOnTouchListener(new SwipeListener()
            {
            @Override
            public void onSimpleTouch()
                {
                setListSingle(relativeLayout, questionList, pos, linearLayout);
                }

            @Override
            public void onLeftToRightSwipe()
                {
                if (isChecked)
                    displayResult(relativeLayout, questionList, pos);
                }

            @Override
            public void onRightToLeftSwipe()
                {
                displayAnswer(relativeLayout, questionList, pos);
                }
            });
       // setArrow(relativeLayout,questionList,pos);

        }
    public void initQuestionTextView(ArrayList<Question> questionList,int i,RelativeLayout rl,
                                     RelativeLayout.LayoutParams RLP)
        {
        TextView question_TV = setTextView(questionList.get(i).arrayQuestion);
           question_TV.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams
                   .FILL_PARENT, 80));
         rl.addView(question_TV, RLP);
        }
    public void initParamImageNonAdmin(ArrayList<Question> questionList, int i,LinearLayout ll,
                                       RelativeLayout rl)
        {

        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(180, 180);
        ImageView img = setImageView(R.drawable.like, questionList.get(i).id, ll);
                ArrayList<Integer> list = LoginActivity.user.sortFavAndQuest(LoginActivity.user
                        .getFavoris());
                for (int l = 0; l < list.size(); l++)          /**Checks if isAldready a favorite**/
                    {
                    if (list.get(l) == questionList.get(i).id)
                        img.setImageResource(R.drawable.likee);
                    }
                param.addRule(RelativeLayout.ALIGN_LEFT);
                param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, rl.getId());
                param.setMargins(35, 100, 0, 30);
                img.setPadding(0,0,0,20);
                rl.addView(img, param);
        }

    public void displayResult(RelativeLayout relativeLayout, ArrayList<Question> questList, int
            position)
        {
        Question          question    = questList.get(position);
        ProgressBar       mProgress   = null;
        ArrayList<String> repVoteList = ToolKit.CheckCharInString('.', question.votes);
        int               nbRep       = question.nbRep;
        for (int i = 0; i < relativeLayout.getChildCount(); i++)
            {
            View child = relativeLayout.getChildAt(i);
            if (child.getClass().equals(CheckBox.class))
                {
                if (nbRep > 0)
                    {
                    mProgress = new ProgressBar(ctx, null, android.R.attr
                            .progressBarStyleHorizontal);
                    setProgBar(child, ctx, mProgress, question, repVoteList, nbRep);
                    ((CheckBox) child).setText(setPourcentage(question.nbTot,
                            Integer.valueOf(repVoteList.get(question.nbRep - nbRep))) + " % ");
                    nbRep--;
                    ViewGroup.LayoutParams params = new ViewGroup.MarginLayoutParams(ViewGroup
                            .LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(350, 70);
                    child.setId(5000 + i);
                    param.addRule(RelativeLayout.ALIGN_TOP, 5000 + i);
                    param.setMargins(600, 0, 00, 0);
                    mProgress.setLayoutParams(param);
                    relativeLayout.addView(mProgress, param);
                    }
                }
            }
        }

    public void setProgBar(View child, Context ctx, ProgressBar mProgress, Question question,
                           ArrayList<String> repVoteList, int nbRep)
        {

        mProgress.setMinimumWidth(400);
        mProgress.setMinimumHeight(20);
        mProgress.setMax(100); // Maximum Progress
        mProgress.setProgressDrawable(ctx.getApplicationContext().getDrawable(R.drawable
                .customprogressbar));
        mProgress.setPadding(10, 30, 0, 0);
        String prc = setPourcentage(question.nbTot,
                Integer.valueOf(repVoteList.get(question.nbRep - nbRep)));
        float prctg = (Float.valueOf(prc));
        int   pourc = (int) prctg;
        setProgColor(mProgress, pourc);
        mProgress.setProgress(pourc);
        mProgress.setSecondaryProgress(pourc);
        ObjectAnimator animation = ObjectAnimator.ofInt(mProgress, "progress", 0, pourc);
        animation.setDuration(1300);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
        mProgress.setOnLongClickListener(new View.OnLongClickListener()
        {
        @Override
        public boolean onLongClick(View v)
            {
            return false;
            }
        });

        }

    public void setProgColor(ProgressBar mProgressBar, int vote)
        {
        switch (vote / 10)
            {
            case 0:
                mProgressBar.getProgressDrawable().setColorFilter(Color.argb(255, 255, 24, 12),
                        PorterDuff.Mode.MULTIPLY);
                break;
            case 1:
                mProgressBar.getProgressDrawable().setColorFilter(Color.argb(255, 255, 24, 12),
                        PorterDuff.Mode.MULTIPLY);
                break;
            case 2:
                mProgressBar.getProgressDrawable().setColorFilter(Color.argb(255, 255, 136, 53),
                        PorterDuff.Mode.MULTIPLY);
                break;
            case 3:
                mProgressBar.getProgressDrawable().setColorFilter(Color.argb(255, 255, 136, 53),
                        PorterDuff.Mode.MULTIPLY);
                break;
            case 4:
                mProgressBar.getProgressDrawable().setColorFilter(Color.argb(255, 255, 242, 66),
                        PorterDuff.Mode.MULTIPLY);
                break;
            case 5:
                mProgressBar.getProgressDrawable().setColorFilter(Color.argb(255, 103, 12, 255),
                        PorterDuff.Mode.MULTIPLY);
                break;
            case 6:

                mProgressBar.getProgressDrawable().setColorFilter(Color.argb(255, 56, 127, 255),
                        PorterDuff.Mode.MULTIPLY);

                break;
            case 7:
                mProgressBar.getProgressDrawable().setColorFilter(Color.argb(255, 30, 245, 255),
                        PorterDuff.Mode.MULTIPLY);
                break;
            case 8:
                mProgressBar.getProgressDrawable().setColorFilter(Color.argb(255, 56, 255, 127),
                        PorterDuff.Mode.MULTIPLY);
                break;
            case 9:
                mProgressBar.getProgressDrawable().setColorFilter(Color.argb(255, 255, 24, 12),
                        PorterDuff.Mode.MULTIPLY);
                break;
            case 10:
                mProgressBar.getProgressDrawable().setColorFilter(Color.argb(255, 255, 24, 12),
                        PorterDuff.Mode.MULTIPLY);
                break;

            }
        }


    public void displayAnswer(RelativeLayout relativeLayout, ArrayList<Question> questList, int
            pos)
        {
        int count = 0;
        for (int i = 0; i < relativeLayout.getChildCount(); i++)
            {
            View child = relativeLayout.getChildAt(i);
            if (child.getClass().equals(CheckBox.class))
                {
                ((CheckBox) child).setText(questList.get(pos).arrayAnswer.get(count));
                count++;
                }
            if (child.getClass().equals(ProgressBar.class))
                {
                child.setVisibility(View.GONE);
                }
            }
        }

    public RelativeLayout setAdminGUI(RelativeLayout relativeLayout, final Question question, final LinearLayout linear)
        {
        final Button buttonPlus  = new Button(ctx);
        final Button buttonMoins = new Button(ctx);
        buttonPlus.setBackgroundResource(R.drawable.pouceplusnoir);
        buttonMoins.setBackgroundResource(R.drawable.poucemoinsnoir);
        Button buttonMid = new Button(ctx);

        buttonPlus.setOnClickListener(new View.OnClickListener()
        {

        @Override
        public void onClick(View v)
            {
            question.validee=true;
            new API(ToolKit.setProgressBar(ctx,linear,null),linear,"wpsquesttable/"+question.getId(),question.QuestToJSON(),4,new API.OnRequestListener()
                   {
        @Override
        public void finish(String result)
            {
            }

        });


                    buttonMoins.setBackgroundResource(R.drawable.poucemoinsnoir);
            buttonPlus.setBackgroundResource(R.drawable.pouceplusvert);
            }
        });
        buttonMoins.setOnClickListener(new View.OnClickListener()
        {
        @Override
        public void onClick(View v)
            {
            buttonPlus.setBackgroundResource(R.drawable.pouceplusnoir);
            buttonMoins.setBackgroundResource(R.drawable.poucemoinsrouge);
            }
        });
        RelativeLayout.LayoutParams paramTotLeft = new RelativeLayout.LayoutParams(ViewGroup
                .LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramTotLeft.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, relativeLayout.getId());
        paramTotLeft.addRule(RelativeLayout.ALIGN_LEFT, relativeLayout.getId());
        paramTotLeft.setMargins(0, 0, 50, 50);
        RelativeLayout.LayoutParams paramTotRight = new RelativeLayout.LayoutParams(ViewGroup
                .LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramTotRight.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, relativeLayout.getId());
        paramTotRight.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, relativeLayout.getId());
        paramTotRight.setMargins(0, 0, 50, 50);
        relativeLayout.addView(buttonPlus, paramTotLeft);
        relativeLayout.addView(buttonMoins, paramTotRight);
        return relativeLayout;
        }

    public void setListSingle(RelativeLayout rl, ArrayList<Question> questionList, int pos,
                              LinearLayout ll)
        {
        if (!isChecked)
            {

            if (ctx == adminActivity.adminContext)
                {
                setAdminGUI(rl,questionList.get(pos),ll);
                }
            setVote(questionList, pos, rl, ll);
            isChecked = true;
            LinearLayout.LayoutParams LP = new LinearLayout.LayoutParams(RelativeLayout
                    .LayoutParams.FILL_PARENT, 400 +
                    questionList.get(pos).arrayAnswer.size() * 130);
            LP.setMargins(0, 0, 0, 32);
            rl.setLayoutParams(LP);
            } else
            {
            LinearLayout.LayoutParams LP = new LinearLayout.LayoutParams(RelativeLayout
                    .LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            LP.setMargins(0, 0, 0, 32);
            rl.setLayoutParams(LP);
            int j = 0;
            for (int i = 0; i < rl.getChildCount(); i++)
                {
                if (rl.getChildAt(i).getClass() == CheckBox.class
                        || rl.getChildAt(i).getClass() == Button.class
                        || rl.getChildAt(i).getClass() == SeekBar.class
                        || rl.getChildAt(i).getClass().equals(ProgressBar.class))
                    {
                    if (rl.getChildAt(i).getClass() == CheckBox.class)
                        {
                        ((CheckBox) rl.getChildAt(i)).setText(questionList.get(pos).arrayAnswer
                                .get(j));
                        j++;
                        }
                    rl.getChildAt(i).setVisibility(View.GONE);
                    questionList.get(pos).isOpened = 1;
                    rl.setPadding(0, 10, 0, 10);
                    }
                }
            isChecked = false;
            }
        }


    public TextView setTextView(String text)
        {
        TextView tv = new TextView(ctx);
        tv.setText(text);
      tv.setTypeface(null, Typeface.BOLD);
       // tv.setBackgroundResource(R.drawable.border);
        tv.setTextSize(19);
        return tv;
        }

    public Button setButton(String text)
        {
        Button butt = new Button(ctx);
        butt.setText(text);
        //butt.setBackgroundResource(R.drawable.border);
        return butt;
        }

    public ImageView setImageView(final int drawable, final int id, final LinearLayout ll)
        {
        final ImageView img = new ImageView(ctx);
        img.setImageResource(drawable);
        img.setClickable(true);
        img.setOnClickListener(new View.OnClickListener()
        {
        @Override
        public void onClick(View v)
            {
            String fav = LoginActivity.user.getFavoris();
            isFav = true;
            if (fav.length() == 0)
                {
                LoginActivity.user.updateFavoris(id);
                img.setImageResource(R.drawable.likee);
                } else
                {
                ArrayList<Integer> intList = LoginActivity.user.sortFavAndQuest(fav);
                for (int p = 0; p < intList.size(); p++)
                    {

                    if (intList.get(p) == id)
                        {
                        isFav = false;
                        LoginActivity.user.updateFavoris(-id);
                        img.setImageResource(R.drawable.like);

                        } else
                        {

                        if (p == intList.size() - 1 && isFav)
                            {
                            LoginActivity.user.updateFavoris(id);
                            img.setImageResource(R.drawable.likee);
                            }
                        }
                    }
                }


            new API(null, ll, "wpsusertable/" + LoginActivity.user.getID(), LoginActivity.user
                    .UserToJSON()
                    , 4, new API.OnRequestListener()

            {
            @Override
            public void finish(String result)
                {
                if (Integer.valueOf(result) != 204)
                    {
                    Toast.makeText(ctx, "Ajout en favoris impossible dû à une erreur serveur:\n " +
                            "E" + result, Toast.LENGTH_LONG).show();

                    } else
                    {
                    if (isFav
                            ) img.setImageResource(R.drawable.likee);
                    else
                        img.setImageResource(R.drawable.like);
                    Toast.makeText(ctx, "Ajout de favoris avec succès!", Toast.LENGTH_SHORT);

                    }

                }
            });
            }
        });

        return img;
        }

    public CheckBox setCheckBox(String text, int vote, final int idQuestion, final int nbRep,
                                final Question question, final LinearLayout ll)
        {
        final CheckBox check = new CheckBox(ctx);
        check.setTextSize(15);
        check.setText(text);
        if (vote != -1)
            {
            check.setChecked(true);
            }
        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
            RelativeLayout rl = (RelativeLayout) check.getParent();
            if (!check.isChecked())
                {

                } else
                {
                question.addVote(nbRep + 1);
                User user = LoginActivity.user;
                user.updateMyReponseList(idQuestion, nbRep + 1);
                new API(null, ll, "wpsusertable/" + user.getID(), user.UserToJSON(), 4, new API
                        .OnRequestListener()

                {
                @Override
                public void finish(String result)
                    {
                    if (Integer.valueOf(result) != 204)
                        Toast.makeText(ctx, "Vote non pris en compte. \n Erreur: " +
                                result, Toast.LENGTH_LONG).show();

                    {

                    }
                    }
                });


                boolean checked = false;
                for (int i = 0; i < rl.getChildCount(); i++)//TODO: prendre en compte désélection
                    {
                    if (rl.getChildAt(i).getClass() == CheckBox.class)
                        {
                        if (((CheckBox) rl.getChildAt(i)).isChecked())
                            {
                            checked = true;
                            ((CheckBox) rl.getChildAt(i)).setChecked(false);

                            }
                        }
                    }
                if (!checked)
                    question.nbTot++;
                new API(null, ll, "wpsquesttable/" + idQuestion, question.QuestToJSON(), 4, new API
                        .OnRequestListener()

                {
                @Override
                public void finish(String result)
                    {

                    }
                });
                check.setChecked(true);
                }
            }

        });
        //check.setTextColor(Color.BLACK);
        check.setLinkTextColor(Color.BLACK);


        return check;
        }

    public void setVote(final ArrayList<Question> array, final int pos, RelativeLayout
            relativeLayout, final LinearLayout ll)
        {
        Question quest = array.get(pos);
        if (quest.isOpened == 1)
            {
            for (int i = 0; i < relativeLayout.getChildCount(); i++)
                {
                if (relativeLayout.getChildAt(i).getClass() == CheckBox.class ||
                        relativeLayout.getChildAt(i).getClass() == Button.class
                        || relativeLayout.getChildAt(i).getClass() == SeekBar.class)
                    {
                    relativeLayout.getChildAt(i).setVisibility(View.VISIBLE);
                    }
                }
            quest.isOpened = 2;
            } else
            {
            buttonMore = setButton("Ajouter une \n réponse");
            buttonMore.setBackgroundResource(R.drawable.border);
            RelativeLayout.LayoutParams pa = new RelativeLayout.LayoutParams(ViewGroup
                    .LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            // pa.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, relativeLayout.getId());
            pa.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, relativeLayout.getId());
            pa.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, relativeLayout.getId());
            pa.setMargins(150, 0, 200, 50);

            //buttonMore.setPadding(0,0,0,50);

            buttonMore.setOnClickListener(new View.OnClickListener()
            {
            @Override
            public void onClick(View v)
                {


                android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7
                        .app.AlertDialog.Builder(ctx);
                final EditText input = new EditText(ctx);
                input.setHint("hint");
                alertDialog.setTitle("Proposez votre solution:");
                alertDialog.setMessage("Notez que pour être acceptée, une réponse doit être écrite " +
                        "dans un langage correct et ne doit pas discriminer une personne ou un produit, " +
                        "mais répondre à la question de l'auteur. Merci.");
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                public void onClick(DialogInterface dialog, int id)
                    {
                    // array.get(pos).arrayAnswer.add((input.getText().toString()));
                    array.get(pos).wait_answer = input.getText().toString() + ".";
                    new API(null, ll, "wpsquesttable/" + array.get(pos).getId(), array.get(pos)
                            .QuestToJSON
                                    (), 4, new API.OnRequestListener()

                    {
                    @Override
                    public void finish(String result)
                        {

                        }
                    });
                    // go to a new activity of the app
                    return;

                    }
                });
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                {
                @Override
                public void onClick(DialogInterface dialog, int which)
                    {
                    return;
                    }
                });
                alertDialog.setView(input);
                //alertDialog.notify();
                alertDialog.show();
                }
            });

            relativeLayout.addView(buttonMore, pa);
           setArrow(relativeLayout, array, pos);

            int idQuestion = array.get(pos).id;
            CheckBox tv2 = null;
            boolean isAnsed = false;
            for (int i = 0; i < array.get(pos).arrayAnswer.size(); i++)
                {
                ArrayList<String> answer = array.get(pos).arrayAnswer;
                ArrayList<Integer> strings = LoginActivity.user.sortReponse()[0];

                boolean isRepondu = false;
                for (int k = 0; k < this.IDquestRepondues.size(); k++)
                    {
                    if (IDquestRepondues.get(k) == idQuestion)
                        isAnsed = true;
                    if ((IDquestRepondues.get(k) == idQuestion) && i == LoginActivity.user
                            .sortReponse()[1].get(k) - 1)
                        {
                        tv2 = setCheckBox(array.get(pos).arrayAnswer.get(i), 1, idQuestion, i,
                                quest, ll);
                        isRepondu = true;
                        }
                    }


                if (!isRepondu)
                    {
                    tv2 = setCheckBox(array.get(pos).arrayAnswer.get(i), -1, idQuestion, i,
                            quest, ll);
                    isRepondu = true;
                    }
                quest.isOpened = 1;
                RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(ViewGroup
                        .LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                int size = array.get(pos).arrayAnswer.size();
                if (i == 0)
                    {
                    //param.addRule(RelativeLayout.BELOW,relativeLayout.getChildAt(1).getId());
                    param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, relativeLayout.getId());
                    param.setMargins(200, 0, 0, 150 + size * 125);
                    } else
                    {
                    param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, relativeLayout.getId());
                    param.setMargins(200, 0, 0, 150 + (size - i) * 125);

                    }
                relativeLayout.addView(tv2, param);
                setProgress(array.get(pos).arrayPourcAnswer, relativeLayout);

            if (!isAnsed)
                quest.nbTot++;

            }
        }}


    public void setProgress(ArrayList<Integer> array, RelativeLayout relativeLayout)
        {
        int tmp    = 0;
        int tmpbis = 0;
        for (int i = 0; i < array.size(); i++)
            {

            SeekBar progBar = new SeekBar(ctx);

            progBar.setProgress(array.get(i));
            progBar.setOnTouchListener(new View.OnTouchListener()
            {
            @Override
            public boolean onTouch(View v, MotionEvent event)
                {
                return true;
                }
            });
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(400, ViewGroup
                    .LayoutParams.WRAP_CONTENT);

            for (int j = 0; j < relativeLayout.getChildCount(); j++)
                {

                if (relativeLayout.getChildAt(j).getClass() == CheckBox.class) // pour aligner
                    // les progressbar aux checkbox
                    {
                    tmpbis++;
                    if (tmpbis > tmp)
                        {
                        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, relativeLayout
                                .getChildAt(j).getId());
                        tmp++;
                        }
                    }
                }
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, relativeLayout.getId());
            params.setMargins(200, 330 - i * 130, 10, 10);

            // relativeLayout.addView(setTextView(array.get(i).toString() + "%"), params);
            // relativeLayout.addView(progBar,params);
            //  params.setMargins(200,300 -i*90,50,25);
            params.addRule(relativeLayout.RIGHT_OF, progBar.getId());

            }
        }


    public String setPourcentage(int nbTot, int nb)
        {
        if (nb == 0 || nbTot == 0)
            {
            return "0";
            } else
            {
            float nbr2 = ((float) nb / (float) nbTot) * 100;
            Float nbr3 = (((float) (((int) (nbr2 * 10f))) / 10f));
            String f = Float.toString(nbr3);//(Math.round(((float) (float) nb / (float) nbTot)) *
            // 100) / 100);
            return f;
            }
        }
    public int getIntFromActivity(Context ctx)
        {
        String activityName=ctx.toString();
        String str= "";

        int result=0;
        if (activityName.contains("MainFlow"))
            {
            return 1;
            }
        if (activityName.contains("Repondue"))
            return 2;
        if (activityName.contains("Favoris"))
            return 3;
        if (activityName.contains("Categorie"))
            return 4;
        /*switch (str)
            {
            case "MainFlowActivity":
                result=1;
                break;
            case "FavorisActivity":
                result=3;
                break;
            default:
                break;

            }*/
        return result;
        }
    public void setRLBackColor(int activity,RelativeLayout relativeLayout)
        {
        switch (activity)
            {
            case 1:
                relativeLayout.setBackgroundResource(R.drawable.border);
                break;
            case 2:
                relativeLayout.setBackgroundResource(R.drawable.border_green);
                break;
            case 3:
                relativeLayout.setBackgroundResource(R.drawable.border_red);
                break;
            case 4:
                relativeLayout.setBackgroundResource(R.drawable.border_yellow);
                break;
            default:
                relativeLayout.setBackgroundResource(R.drawable.border);
                break;
            }
        }

    }

