package com.example.miyke.wps.Class;

import com.example.miyke.wps.InternetClass.Internet;
import com.example.miyke.wps.LoginActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by miyke on 07/01/16.
 */
public class User

    {
    private String username, mdp, my_questionList, myReponseList, favoris, email,wait_rep,status;
    private int nbRep, nbQuest, id;


    public User(String userName, String mdp, String my_questions, String my_reponse, int nbRep,
                int nbQuest, String favoris, String email, int id,String status)
        {
        this.status=status;
        this.username = userName;
        this.mdp = mdp;
        this.my_questionList = my_questions;
        this.myReponseList = my_reponse;
        this.nbQuest = nbQuest;
        this.nbRep = nbRep;
        this.favoris = favoris;
        this.email = email;
        this.id = id;
        }


    public String getUsername()
        {
        return this.username;
        }

    public String getMy_questionList()
        {
        return this.my_questionList;
        }

    public String getMyReponseList()
        {
        return this.myReponseList;
        }

    public int getNbRep()
        {
        return this.nbRep;
        }

    public int getNbQuest()
        {
        return this.nbQuest;
        }

    public void updatenbRep()
        {
        this.nbRep += 1;
        }

    public void updateNbQuest()
        {
        this.nbQuest += 1;
        }

    public String getMdp()
        {
        return this.mdp;
        }

    public void updateFavoris(int idQuest)
        {
        if (idQuest > 0)
            this.favoris += idQuest + ";";
        else
            {
            String tmp = "";
            ArrayList<Integer> favorites = LoginActivity.user.sortFavAndQuest(LoginActivity.user
                    .getFavoris());
            for (int i = 0; i < favorites.size(); i++)
                {

                if (-idQuest != favorites.get(i))
                    {
                    tmp += favorites.get(i).toString() + ';';
                    }
                }
            favoris = tmp;
            }
        }

    public String getFavoris()
        {
        return favoris;
        }

    public int getID()
        {
        return id;
        }

    public void updateMyQuestionList(int idQuest)
        {
        this.my_questionList += idQuest + ";";
        updateNbQuest();
        }

    public void updateMyReponseList(int idQuest, int repChoisi)
        {
        ArrayList<Integer>[] array = sortReponse();
        String rep = "";
        for (int i = 0; i <= array[0].size(); i++)
            {
            if (i < array[0].size())
                {
                if (array[0].get(i) == idQuest)
                    {
                    array[1].set(i, repChoisi);
                    nbRep--;
                    }
                else
                    rep += array[0].get(i).toString() + ":" + array[1].get(i).toString() + ";";
                }
            else
                {
                rep += Integer.toString(idQuest) + ":" + repChoisi + ";";
                }

            }

        this.myReponseList = rep;//Integer.toString(idQuest) + ":" + repChoisi + ";";
        updatenbRep();
        }

    public ArrayList<Integer> sortFavAndQuest(String FavAndQuest)
        {
        ArrayList<Integer> list = new ArrayList<>();
        ArrayList<String> stringArray = ToolKit.CheckCharInString(';', FavAndQuest);
        for (int i = 0; i < stringArray.size(); i++)
            {
            list.add(Integer.valueOf(stringArray.get(i)));
            }
        return list;
        }

    public ArrayList<Integer>[] sortReponse()
        {
        ArrayList<Integer>[] array;
        array = new ArrayList[2];
        array[0] = new ArrayList<Integer>();
        array[1] = new ArrayList<>();
        ArrayList<String> list = ToolKit.CheckCharInString(';', this.myReponseList);
        for (int i = 0; i < list.size(); i++)
            {
            String quest = (ToolKit.CheckCharInString(':', list.get(i)).get(0));
            array[0].add(Integer.valueOf(quest));
            array[1].add(Integer.valueOf(list.get(i).substring(array[0].get(i).toString().length
                            () + 1,
                    list.get(i).length())));
            }

        return array;
        }

    public JSONObject UserToJSON()
        {
        JSONObject jobj = new JSONObject();
        try
            {
            jobj.put("pseudo", getUsername());
            jobj.put("mdp", mdp);
            jobj.put("favoris", favoris);
            jobj.put("nbQuestion", nbQuest);
            jobj.put("nbReponse", nbRep);
            jobj.put("myQuestions", my_questionList);
            jobj.put("myReponses", myReponseList);
            jobj.put("email", email);
            jobj.put("id", id);
            jobj.put("status",status);

            } catch (JSONException e)
            {
            e.printStackTrace();
            }
        return jobj;
        }
    }
