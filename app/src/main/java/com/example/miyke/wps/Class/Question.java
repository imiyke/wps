package com.example.miyke.wps.Class;

import com.example.miyke.wps.InternetClass.Internet;
import com.example.miyke.wps.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by miyke on 19/10/15.
 */
public class Question
    {
    public int isOpened;
    public int totAns, nbFav;
    public int id, nbRep, nbTot;
    public boolean validee;
    ArrayList<String> arrayAnswer;
    String            arrayQuestion, category, wait_answer, votes;
    ArrayList<Integer> arrayPourcAnswer;


    public Question(String arrayList, ArrayList<String> answer, ArrayList<Integer> pourcAnswer,
                    int totAnswer, int idQuest, int nbRep, boolean validee, String category, int
                            nbreFav, String vote)
        {
        arrayQuestion = arrayList;
        arrayAnswer = answer;
        arrayPourcAnswer = pourcAnswer;
        nbTot = totAnswer;
        nbFav = nbreFav;
        votes = vote;
        isOpened = 0;
        id = idQuest;
        totAns = totAnswer;
        this.category = category;
        this.validee = validee;
        this.nbRep = nbRep;

        }

    public void addVote(int reponse)
        {
        ArrayList<Integer> IDquestRepondues = LoginActivity.user.sortReponse()[0];
        int    i      = 0;
        int    tmp    = 0;
        int    l      = 0;
        String tmpStr = "";
        int oldresponse=0;
        int reponse2=reponse;
        ArrayList<String> repNbVoteList=ToolKit.CheckCharInString('.',votes);
        for (int m=0;m<IDquestRepondues.size();m++)
            {
            if (IDquestRepondues.get(m)==this.getId())
                {
                oldresponse=LoginActivity.user.sortReponse()[1].get(m);
                }
            else
                {
                nbTot++;
                }
            }
        for (int plop=0;plop<repNbVoteList.size();plop++)
            {
            if (plop+1==reponse)
                {
                repNbVoteList.set(plop,Integer.toString(Integer.valueOf(repNbVoteList.get(plop)) + 1));
                }
            if (plop+1==oldresponse)
                {
                repNbVoteList.set(plop,Integer.toString(Integer.valueOf(repNbVoteList.get(plop))-1));
                }
            }
        votes=ToolKit.ListToString(repNbVoteList,'.');

        }

    public int getId()
        {
        return id;
        }

    public String getQuestion()
        {
        return this.arrayQuestion;
        }

    public JSONObject QuestToJSON()
        {
        JSONObject jobj = new JSONObject();
        String     tmp  = "";
        for (int i = 0; i < this.arrayAnswer.size(); i++)
            {
            tmp += arrayAnswer.get(i).toString() + ".";
            }
        try
            {
            jobj.put("nbReponse", this.nbRep);
            jobj.put("nbfavoris", nbFav);

            jobj.put("nbTotal", nbTot);
            jobj.put("votes", votes);
            jobj.put("question", this.arrayQuestion);
            jobj.put("reponse", tmp);
            jobj.put("validee", this.validee);
            jobj.put("category", this.category);
            jobj.put("id", this.id);
            if (wait_answer != null)
                jobj.put("waitReponse", wait_answer);
            }
        catch (JSONException e)
            {
            e.printStackTrace();
            }
        return jobj;
        }
    }
