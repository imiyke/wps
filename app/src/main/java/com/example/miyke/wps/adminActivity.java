package com.example.miyke.wps;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;

import com.example.miyke.wps.Class.Question;
import com.example.miyke.wps.Class.ToolKit;
import com.example.miyke.wps.Class.WebToolKit;

import java.util.ArrayList;


public class adminActivity extends ActionBarActivity
    {

    private CheckBox cB1, cB2;
    private LinearLayout adminll;
    public static Context adminContext;
    private ProgressBar pb;
    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        cB1=(CheckBox)findViewById(R.id.admincb1);
        cB2=(CheckBox)findViewById(R.id.admincb2);
        adminll=(LinearLayout)findViewById(R.id.admin_ll);
        adminContext=this;
        pb= ToolKit.setProgressBar(this,adminll,pb);
        cB1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
            if (cB2.isChecked())
                {
                cB2.setChecked(false);
                adminll.removeAllViews();
                }
            if (isChecked)
                {
                WebToolKit.APIConnect("wpsquesttable/validee/false",adminll,adminContext,pb);
                cB1.setChecked(true);
                }
             }
        });
        cB2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
            if (cB1.isChecked())
                {
                cB1.setChecked(false);
                adminll.removeAllViews();
                }
           if (isChecked)
               {
               cB2.setChecked(true);
               //WebToolKit.APIConnect() TODO: download and display wait_reponse
               }

            }
        });

        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
        {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_admin, menu);
        return true;
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
        {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            {
            return true;
            }

        return super.onOptionsItemSelected(item);
        }
    }
