package com.example.miyke.wps;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.miyke.wps.Class.Question;
import com.example.miyke.wps.Class.ToolKit;
import com.example.miyke.wps.Class.User;
import com.example.miyke.wps.Class.WebToolKit;
import com.example.miyke.wps.InternetClass.API;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//TODO: handle PostRsquest to Server in order to create a question.
public class AddQuestionActivity extends ActionBarActivity
        implements CustomNavigationDrawerFragment.NavigationDrawerCallbacks
    {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private CustomNavigationDrawerFragment mCustomNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private EditText addQuest_quest;
    private RelativeLayout addQuest_RL;
    private Button buttEnvoyer, buttValider;
    private NumberPicker noPicker;
    private boolean needToReload = false;
    private int nbRep = 0;
    private Spinner spinner = null;
    public static Context AddContext;
    public static Boolean questionAded=true;
    private ProgressBar progressBar;
    private LinearLayout ll;


    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_question);
        this.AddContext=this;
        mCustomNavigationDrawerFragment = (CustomNavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mCustomNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        //Set up fields/widgets
        addQuest_RL = (RelativeLayout) findViewById(R.id.addQuest_RL);
      //  buttValider = (Button) findViewById(R.id.addQuest_buttValider);
        addQuest_quest = (EditText) findViewById(R.id.addQuest_quest);
        ll=(LinearLayout)findViewById(R.id.lineardesign);
        buttEnvoyer = (Button) findViewById(R.id.addQuest_buttEnvoyer);
        buttEnvoyer.setOnClickListener(new View.OnClickListener()
        {
        @Override
        public void onClick(View v)
            {
            addQuestion(AddContext,ll);
            }
        });
        setSpinnerCategory();
        setNumberPicker();
        //setProgressBar();
       // progressBar=ToolKit.setProgressBar(this,(LinearLayout)findViewById(R.id.lineardesign),progressBar);


        }

    public void setNumberPicker()
        {
        noPicker = (NumberPicker) findViewById(R.id.numberPicker);
        noPicker.setMaxValue(5);
        noPicker.setMinValue(0);
       // noPicker.setWrapSelectorWheel(true);
        noPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
        {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {
            if (needToReload)
                {
                for (int i = 0; i < nbRep; i++)
                    {
                    // addQuest_RL.removeViewAt(500 + i);
                    addQuest_RL.removeView((View) findViewById(500 + i));
                    }
                needToReload = false;
                }
             nbRep = noPicker.getValue();
            setAnswerSpace(nbRep);
            buttEnvoyer.setEnabled(true);
            needToReload = true;
            }
        });
       /* buttValider.setOnClickListener(new View.OnClickListener()
        {
        @Override
        public void onClick(View v)
            {
            if (needToReload)
                {
                for (int i = 0; i < nbRep; i++)
                    {
                    // addQuest_RL.removeViewAt(500 + i);
                    addQuest_RL.removeView((View) findViewById(500 + i));
                    }
                needToReload = false;
                }



            nbRep = noPicker.getValue();
            setAnswerSpace(nbRep);
            buttEnvoyer.setEnabled(true);
            needToReload = true;

            }
        });*/
    /*    noPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
        {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {
            buttValider.setEnabled(true);
            }
        });*/

        }

    public void setAnswerSpace(int nb) //Cree et positionne les champs de reponses
    {
    for (int i = 0; i < nb; i++)
        {
        EditText reponse_i = new EditText(this);
        RelativeLayout.LayoutParams RLP = new RelativeLayout.LayoutParams(ViewGroup
                .LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        RLP.setMargins(10,10,10,20);
        reponse_i.setHint("Reponse " + (i + 1));
        reponse_i.setId(500 + i);
        if (i == 0)
            RLP.addRule(RelativeLayout.BELOW, R.id.numberPicker);
        else
            RLP.addRule(RelativeLayout.BELOW, 500 + i - 1);
        addQuest_RL.addView(reponse_i, RLP);
        }
    }

    public void setSpinnerCategory() //Crée le spinner et définit la liste de Categories possibles.
    {
    spinner = (Spinner) findViewById(R.id.spinner);


    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_spinner_item, MainFlowActivity.listCategorie);
    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spinner.setAdapter(dataAdapter);
    }

    public void addQuestion(Context ctx, LinearLayout ll) //a reformatter
        {
        String reponse = "";
        progressBar=ToolKit.setProgressBar(ctx,ll,progressBar);
        boolean questionIsOK = true;
        for (int i = 0; i < nbRep; i++)
            {
            String reponseTmp = ((EditText) findViewById(500 + i)).getText().toString();
            if (ToolKit.CheckCharInString('.', reponseTmp).size() != 0 ||
                    ToolKit.CheckCharInString('\n',reponseTmp).size()!=0)
                {
                Toast.makeText(this, "Mauvais format de réponse. Veillez à ce qu'il n'y ait pas " +
                        "de point ni de retour à la ligne dans les réponses proposées.", Toast.LENGTH_LONG).show();
                questionIsOK = false;
                }

            reponse += reponseTmp + ".";
            }
        if (questionIsOK)
            {
            final JSONObject jobj = new JSONObject();
            try
                {
                String vote="";
                jobj.put("nbReponse", nbRep);
                while(nbRep>0)
                    {
                    vote+="0.";
                    nbRep--;
                    }
                jobj.put("votes",vote);
                jobj.put("nbTotal", 0);
                jobj.put("nbfavoris", 0);
                jobj.put("question", addQuest_quest.getText().toString());
                jobj.put("reponse", reponse);
                jobj.put("validee", false);
                jobj.put("wait_reponse", "");
                jobj.put("category", spinner.getSelectedItem().toString());
                //  [{"id":4,"nbReponse":é,"nbTotal":0,"nbVote":0,"question":"Est-ce que ça marche,
                // tercio?","reponse":"UN! . deux !","validee":true}]
                } catch (JSONException e)
                {
                e.printStackTrace();
                }
            final Intent intent = new Intent(this, MainFlowActivity.class);
            intent.putExtra("Question",addQuest_quest.getText().toString());
            API addQuestionAPI = new API(progressBar,ll,"wpsquesttable/create", jobj, 1, new API.OnRequestListener()
            {
            @Override
            public void finish(String result)
                {
                try
                    {
                    JSONObject jsonObject=new JSONObject(result);
                     LoginActivity.user.updateMyQuestionList(jsonObject.getInt("id"));
                    //LoginActivity.user.updateNbQuest();
                    } catch (JSONException e)
                    {
                    e.printStackTrace();
                    }
                CustomNavigationDrawerFragment.NavDrawerPosition=0;
                startActivity(intent);
                }
            });
            }
        }




    @Override
    public void onNavigationDrawerItemSelected(int position)
        {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
        }

    public void onSectionAttached(int number)
        {
         Intent intent=null;
        switch (number)
            {

            case 1:
                intent=new Intent(this,MainFlowActivity.class);
                break;
            case 2:
                intent=new Intent(this,Categories.class);
                break;
            case 3:
                intent=new Intent(this,FavorisActivity.class);
                break;
            case 4:
                intent=new Intent(this,MyQuestionActivity.class);
                 break;
            case 5:
                intent=new Intent(this,ReponduesActivity.class);
                  break;
            case 6:
                intent=new Intent(this,FriendsActivity.class);
                break;

            }
        if (intent!=null)
          startActivity(intent);

        }

    public void restoreActionBar()
        {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
        {
        if (!mCustomNavigationDrawerFragment.isDrawerOpen())
            {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.add_question, menu);
            restoreActionBar();
            return true;
            }
        return super.onCreateOptionsMenu(menu);
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
        {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            {
            return true;
            }

        return super.onOptionsItemSelected(item);
        }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment
        {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber)
            {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
            }

        public PlaceholderFragment()
            {
            }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
            {
            View rootView = inflater.inflate(R.layout.fragment_add_question, container, false);
            return rootView;
            }

        @Override
        public void onAttach(Activity activity)
            {
            super.onAttach(activity);
            ((AddQuestionActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
            }
        }

    }
