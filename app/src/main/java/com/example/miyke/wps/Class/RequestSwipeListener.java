package com.example.miyke.wps.Class;

/**
 * Created by miyke on 21/01/16.
 */
public interface RequestSwipeListener
    {
    void onLeftToRightSwipe();

    void onRightToLeftSwipe();

    void  onSimpleTouch();
    }
