package com.example.miyke.wps.Class;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.miyke.wps.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by miyke on 05/01/16.
 */
public class ToolKit
    {
    public static boolean isLoading=false;
    public static ArrayList<String> CheckCharInString(char ch,String str)
        {
        ArrayList<String> list=new ArrayList<>();
        int j=0;
        for (int i=0;i<str.length();i++)
            {
            if (str.charAt(i)==ch)
                {
                list.add(str.substring(j,i));
                j=i+1;
                }
            }
        return list;
        }
    public static String ListToString(ArrayList<String>  strList,char ch)
        {
        String str="";
        for (int i=0;i<strList.size();i++)
            {
            str+=strList.get(i)+ch;
            }
        return str;
        }
    public static JSONObject FillJSONObjectFromString(ArrayList<String> listNames,
                                                      ArrayList<String> listValues)
        {
        JSONObject jobj=new JSONObject();
        for (int i=0;i<listNames.size();i++)
            {
            try
                {
                jobj.put(listNames.get(i),listValues.get(i));
                }
            catch (JSONException e)
                {
                e.printStackTrace();
                }
            }
        return jobj;
        }
    public static ProgressBar setProgressBar(Context ctx, LinearLayout linearLayout,ProgressBar pBar)
        {

        /*if (LoginActivity.test)
            {
//            pBar.setVisibility(View.VISIBLE);
           // linearLayout.invalidate();
//            linearLayout.addView(pBar);
            return pBar;
            }
        else*/
            {
             pBar =new ProgressBar(ctx, null, android.R.attr.progressBarStyle);
            linearLayout.addView(pBar);
            LoginActivity.test=true;
           // secondBar.setVisibility(View.GONE);
            return pBar;
            }
        }
    public static void stopProgressBar(ProgressBar pb)
        {

        pb.setVisibility(View.GONE);
        LoginActivity.test=false;

        }

    }
