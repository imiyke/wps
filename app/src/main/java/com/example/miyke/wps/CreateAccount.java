package com.example.miyke.wps;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.miyke.wps.Class.ToolKit;
import com.example.miyke.wps.InternetClass.API;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.PendingIntent.getActivity;


public class CreateAccount extends ActionBarActivity
    {
private Button createAccountButton;
    private String mail, pass, pseudo;
    private boolean compteExistence = false;
    private LinearLayout createll;
    private Context      ctx;
    private ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        // compteExistence=true;
        createAccountButton = (Button) findViewById(R.id.create_ValidButton);
        createll = (LinearLayout) findViewById(R.id.create_ll);
        pb= ToolKit.setProgressBar(this,createll,pb);
        pb.setVisibility(View.GONE);
        ctx = this.getApplicationContext();
        createAccountButton.setOnClickListener(new View.OnClickListener()
        {

        @Override
        public void onClick(View v)
            {
            mail = ((EditText) findViewById(R.id.create_Mail)).getText().toString();
            pass = ((EditText) findViewById(R.id.create_Pass)).getText().toString();
            pseudo = ((EditText)findViewById(R.id.create_Pseudo)).getText().toString();
            JSONObject jobj=new JSONObject();
            try
                {
                jobj.put("pseudo",pseudo);jobj.put("mdp",pass);jobj.put("nbReponse",0);
                jobj.put("nbQuestion",0);jobj.put("email",mail);
                }
            catch (JSONException e)
                {
                e.printStackTrace();
                }
                if (!compteExistence)
                    {
                    final Intent intent = new Intent(CreateAccount.this, LoginActivity.class);
                    pb.setVisibility(View.VISIBLE);
                    new API(pb,createll,"wpsusertable", jobj, 1, new API.OnRequestListener()
                    {
                    @Override
                    public void finish(String result)
                        {
                        Toast.makeText(CreateAccount.this,"Compte créé avec succès",Toast.LENGTH_LONG).show();
                        startActivity(intent);
                        }
                    });
                    }
                else
                    {
                    Toast.makeText(CreateAccount.this, "Ce compte existe déjà.", Toast.LENGTH_LONG).show();
                    }
            }
        });
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
        {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_account, menu);
        return true;
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
        {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            {
            return true;
            }

        return super.onOptionsItemSelected(item);
        }
    }
