package com.example.miyke.wps;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.miyke.wps.Class.CustomListView;
import com.example.miyke.wps.Class.Question;
import com.example.miyke.wps.Class.ToolKit;
import com.example.miyke.wps.Class.WebToolKit;

import java.util.ArrayList;


public class Categories extends ActionBarActivity
        implements CustomNavigationDrawerFragment.NavigationDrawerCallbacks
    {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private CustomNavigationDrawerFragment mCustomNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private LinearLayout ll;
    private ArrayList<Question> list         = null;
    private Spinner             CategSpinner = null;
    private String              category     = "";
    private Boolean             needToReload = false;
    private ProgressBar pb;
    private Context     ctx;
    private int         getCateg;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        /** Init sequence **/
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        this.ctx = this;
        mCustomNavigationDrawerFragment = (CustomNavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mCustomNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        getCateg = this.getIntent().getIntExtra("categorie", 0);
        ll = (LinearLayout) findViewById(R.id.categ_ll);
        LinearLayout categLoad = (LinearLayout) findViewById(R.id.categ_ll_Load);
        pb = ToolKit.setProgressBar(this, categLoad, pb);
        pb.setVisibility(View.VISIBLE);

        /**Connexion sequence**/

        list = WebToolKit.APIConnect("wpsquesttable/category/" + setCategory(getCateg), ll, ctx,
                pb);
        }

    public String setCategory(int pos)
        {
        String category = "";
        switch (pos)
            {
            case 20:
                category = MainFlowActivity.listCategorie.get(0);
                break;
            case 21:
                category = MainFlowActivity.listCategorie.get(1);
                break;
            case 22:
                category = MainFlowActivity.listCategorie.get(2);
                break;
            case 23:
                category = MainFlowActivity.listCategorie.get(3);
                break;
            case 24:
                category = MainFlowActivity.listCategorie.get(4);
            }
        return category;
        }

    @Override
    public void onNavigationDrawerItemSelected(int position)
        {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
        }

    public void onSectionAttached(int number)
        {
        Intent intent = null;
        switch (number)
            {

            case 1:
                intent = new Intent(this, MainFlowActivity.class);
                startActivity(intent);
                break;
            case 2:
                // intent=new Intent(this,Categories.class);
                break;
            case 3:
                intent = new Intent(this, FavorisActivity.class);
                startActivity(intent);
                break;
            case 4:
                intent = new Intent(this, MyQuestionActivity.class);
                startActivity(intent);
                break;
            case 5:
                intent = new Intent(this, ReponduesActivity.class);
                startActivity(intent);
                break;
            case 7:
                intent = new Intent(this, AddQuestionActivity.class);
                startActivity(intent);
                break;
            case 6:
                intent = new Intent(this, FriendsActivity.class);
                startActivity(intent);
                break;

            }

        }

    public void restoreActionBar()
        {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
        {
        if (!mCustomNavigationDrawerFragment.isDrawerOpen())
            {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.categories, menu);
            restoreActionBar();
            return true;
            }
        return super.onCreateOptionsMenu(menu);
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
        {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            {
            return true;
            }

        return super.onOptionsItemSelected(item);
        }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment
        {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber)
            {

            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle              args     = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
            }

        public PlaceholderFragment()
            {
            }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
            {
            View rootView = inflater.inflate(R.layout.fragment_categories, container, false);
            return rootView;
            }

        @Override
        public void onAttach(Activity activity)
            {
            super.onAttach(activity);
            ((Categories) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
            }
        }

    }
