package com.example.miyke.wps.Class;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

/**
 * Created by miyke on 21/01/16.
 */

public abstract class SwipeListener implements View.OnTouchListener, RequestSwipeListener
    {
    private int min_distance = 50;
    private float downX, downY, upX, upY;
    View v;

    @Override
    public boolean onTouch(View v, MotionEvent event)
        {
        this.v = v;
        switch (event.getAction())
            { // Check vertical and horizontal touches
            case MotionEvent.ACTION_DOWN:
            {
            downX = event.getX();
            downY = event.getY();
            return true;

            }
            case MotionEvent.ACTION_UP:
            {
            upX = event.getX();
            upY = event.getY();

            float deltaX = downX - upX;
            float deltaY = downY - upY;
            if (deltaX==0 && deltaY==0)
                {
                onSimpleTouch();
                return true;
                }

//HORIZONTAL SCROLL
            if (Math.abs(deltaX) > Math.abs(deltaY))
                {
                if (Math.abs(deltaX) > min_distance)
                    {
// left or right
                    if (deltaX < 0)
                        {
                        this.onLeftToRightSwipe();
                        return true;
                        }
                    if (deltaX > 0)
                        {
                        this.onRightToLeftSwipe();
                        return true;
                        }
                    } else
                    {
//not long enough swipe...
                    //this.onSimpleTouch();
                    return false;
                    }
                }
//VERTICAL SCROLL
            else
                {
                if (Math.abs(deltaY) > min_distance)
                    {
// top or down
                    if (deltaY < 0)
                        {
                        this.onTopToBottomSwipe();
                        return true;
                        }
                    if (deltaY > 0)
                        {
                        this.onBottomToTopSwipe();
                        return true;
                        }
                    } else
                    {
//not long enough swipe...
                   // this.onSimpleTouch();
                    return false;
                    }
                }

            return false;
            }
            }
       // this.onSimpleTouch();
        return false;
        }

   // @Override
    public abstract void onLeftToRightSwipe();



    @Override
    public void onRightToLeftSwipe()
        {
        Toast.makeText(v.getContext(), "right to left",
                Toast.LENGTH_SHORT).show();
        }

    public void onTopToBottomSwipe()
        {
        Toast.makeText(v.getContext(), "top to bottom",
                Toast.LENGTH_SHORT).show();
        }

    public void onBottomToTopSwipe()
        {
        Toast.makeText(v.getContext(), "bottom to top",
                Toast.LENGTH_SHORT).show();
        }

   // @Override
    public void  onSimpleTouches()
        {
        Toast.makeText(v.getContext(),"Simple Touch",Toast.LENGTH_LONG).show();
        }

    public  abstract void onSimpleTouch();
    }
