package com.example.miyke.wps;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.miyke.wps.Class.CustomListView;
import com.example.miyke.wps.Class.Question;
import com.example.miyke.wps.Class.ToolKit;
import com.example.miyke.wps.Class.WebToolKit;
import com.example.miyke.wps.InternetClass.API;
import com.example.miyke.wps.InternetClass.Internet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MyQuestionActivity extends ActionBarActivity
        implements CustomNavigationDrawerFragment.NavigationDrawerCallbacks
    {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private CustomNavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private LinearLayout questionll;
    private ArrayList<Question> list = null;
    private       Button  refresh;
    public static Context ctx;
    private ProgressBar pBar = null;
    //private LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        /** ====================== INIT SEQUENCE ===============================================**/

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_question);
        final LinearLayout ll = (LinearLayout) findViewById(R.id.questionll);
        mNavigationDrawerFragment = (CustomNavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        ctx = this;
        pBar = ToolKit.setProgressBar(this, ll, pBar);
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        /** ========== TAKE USER QUESTIONS ====================**/

        ArrayList<Question>      listQuest   = new ArrayList<>();
        final ArrayList<Integer> myquestList = LoginActivity.user.sortFavAndQuest(LoginActivity
                .user.getMy_questionList());

        /** ====================== ACTION SEQUENCE- DOWNLOAD USER QUESTIONS ====================**/
        final JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < myquestList.size(); i++)
            {
            final int j = i;
            API testAPI = new API(pBar, ll, "wpsquesttable/" + myquestList.get(i).toString(),
                    null, 2, new API.OnRequestListener()
            {
            @Override
            public void finish(String result)
                {

                try
                    {
                    jsonArray.put(new JSONObject(result));
                    }
                catch (JSONException e)
                    {
                    e.printStackTrace();
                    }
                if (j == myquestList.size() - 1)
                    {

                    /** ========== CREATE LISTVIEW WITH DOWNLOADED DATAS ====================== **/

                    list = WebToolKit.LoadQuestionTab(jsonArray, ctx, (LinearLayout) findViewById
                            (R.id.questionll));
                    Toast.makeText(MyQuestionActivity.this, "cest finiiiiii", Toast.LENGTH_LONG)
                            .show();
                    }
                }
            });
            }
        }

    @Override
    public void onNavigationDrawerItemSelected(int position)
        {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
        }

    public void onSectionAttached(int number)
        {
       /* Intent intent = null;
        switch (number)
            {

            case 1:
                intent = new Intent(this, MainFlowActivity.class);
                break;
            case 2:
                intent = new Intent(this, Categories.class);
                break;
            case 3:
                intent = new Intent(this, FavorisActivity.class);
                break;
            case 4:
                //intent=new Intent(this,MyQuestionActivity.class);
                break;
            case 5:
                intent = new Intent(this, ReponduesActivity.class);
                break;
            case 6:
                intent = new Intent(this, FriendsActivity.class);
                break;
            case 7:
                intent = new Intent(this, AddQuestionActivity.class);
                break;

            }
        if (intent != null)
            startActivity(intent);*/

        }

    public void restoreActionBar()
        {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
        {
        if (!mNavigationDrawerFragment.isDrawerOpen())
            {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.my_question, menu);
            restoreActionBar();
            return true;
            }
        return super.onCreateOptionsMenu(menu);
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
        {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            {
            return true;
            }

        return super.onOptionsItemSelected(item);
        }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment
        {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber)
            {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle              args     = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
            }

        public PlaceholderFragment()
            {
            }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
            {
            View rootView = inflater.inflate(R.layout.fragment_my_question, container, false);
            return rootView;
            }

        @Override
        public void onAttach(Activity activity)
            {
            super.onAttach(activity);
            ((MyQuestionActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
            }
        }

    }
