package com.example.miyke.wps;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.miyke.wps.Class.Question;
import com.example.miyke.wps.Class.ToolKit;
import com.example.miyke.wps.Class.WebToolKit;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class FriendsActivity extends ActionBarActivity
        implements com.example.miyke.wps.CustomNavigationDrawerFragment.NavigationDrawerCallbacks
    {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private CustomNavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private LinearLayout ll = null;
    private Context ctx;
    private ArrayList<Question> list=null;
    private ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        /** _____________________________________ INIT SEQUENCE ________________________________**/

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        mNavigationDrawerFragment = (CustomNavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        this.ctx = this;
        ll = (LinearLayout) findViewById(R.id.friends_ll);
        pb= ToolKit.setProgressBar(this,ll,pb);

        /** ___________________________________ CONNECT SEQUENCE ________________________________**/

        list = WebToolKit.APIConnect("wpsquesttable/amis/2" , ll, ctx,pb);

        }

    @Override
    public void onNavigationDrawerItemSelected(int position)
        {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
        }

    public void onSectionAttached(int number)
        {

         Intent intent=null;
        switch (number)
            {

            case 1:
                intent=new Intent(this,MainFlowActivity.class);
                break;
            case 2:
                intent=new Intent(this,Categories.class);
                break;
            case 3:
                intent=new Intent(this,FavorisActivity.class);
                break;
            case 4:
                intent=new Intent(this,MyQuestionActivity.class);
                break;
            case 5:
                intent=new Intent(this,ReponduesActivity.class);
                break;
            case 7:
                intent=new Intent(this,AddQuestionActivity.class);

            }
        if (intent!=null)
            startActivity(intent);
        }

    public void restoreActionBar()
        {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
        {
        if (!mNavigationDrawerFragment.isDrawerOpen())
            {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.friends, menu);
            restoreActionBar();
            return true;
            }
        return super.onCreateOptionsMenu(menu);
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
        {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            {
            return true;
            }

        return super.onOptionsItemSelected(item);
        }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment
        {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber)
            {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle              args     = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
            }

        public PlaceholderFragment()
            {
            }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
            {
            View rootView = inflater.inflate(R.layout.fragment_friends, container, false);
            return rootView;
            }

        @Override
        public void onAttach(Activity activity)
            {
            super.onAttach(activity);
            ((FriendsActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
            }
        }

    }
