package com.example.miyke.wps.Class;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.miyke.wps.AddQuestionActivity;
import com.example.miyke.wps.InternetClass.API;
import com.example.miyke.wps.MainFlowActivity;
import com.example.miyke.wps.MyQuestionActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by miyke on 08/12/15.
 */
public class WebToolKit
    {
    public static JSONArray JArray = null;

    public static ArrayList<Question> APIConnect(String partURL, final LinearLayout linearLayout,
                                                 final Context ctx, final ProgressBar pb)
        {


        JArray = new JSONArray();
        API plop = new API(pb, linearLayout, partURL, null, 2, new API.OnRequestListener()
        {
        @Override
        public void finish(String result)
            {
            JSONArray JArray2 = null;
            try
                {
                JArray2 = new JSONArray(result);
                JArray = JArray2;
                LoadQuestionTab(JArray, ctx, linearLayout);
                }
            catch (JSONException e)
                {
                e.printStackTrace();
                }
            }

        });
        ArrayList<Question> list = new ArrayList<>();
        return list;
        }


    public static ArrayList<Question> LoadQuestionTab(JSONArray jsonArray, Context ctx,
                                                      LinearLayout linear)
        {

        ArrayList<Question> list = new ArrayList<>();
        int                 i    = 0;
        if (jsonArray != null)
            {
            int nbfav = 0;
            try
                {
                while (jsonArray.getJSONObject(i) != null)
                    {
                    ArrayList<String> stringTab = ToolKit.CheckCharInString('.',
                            jsonArray.getJSONObject(i).getString("reponse"));
                    int NbTot = jsonArray.getJSONObject(i).getInt("nbTotal");
                    String Quest = jsonArray.getJSONObject(i).getString("question");
                    int idQuest = jsonArray.getJSONObject(i).getInt("id");
                    int nbRep = jsonArray.getJSONObject(i).getInt("nbReponse");
                    String categ = jsonArray.getJSONObject(i).getString("category");
                    String vote = jsonArray.getJSONObject(i).getString("votes");
                    nbfav = jsonArray.getJSONObject(i).getInt("nbfavoris");
                    Boolean valid = jsonArray.getJSONObject(i).getBoolean("validee");
                    Question question = new Question(Quest, stringTab, getPourc("plop"), NbTot,
                            idQuest, nbRep, valid, categ, nbfav, vote);
                    list.add(question);
                    i++;
                    }
                }
            catch (JSONException e)
                {
                e.printStackTrace();
                }
            }

        if (ctx != AddQuestionActivity.AddContext)
            new CustomListView(list, linear, ctx);
        return list;
        }

    public static ArrayList<String> sortAnwer(String anwser)
        {
        ArrayList<String> reponseTab = new ArrayList<>();
        for (int i = 0; i < anwser.length(); i++)
            {
            if (anwser.charAt(i) == '.')
                {
                String s2 = anwser.substring(0, (anwser.length() >= 1) ? i : 0);
                anwser = anwser.substring(s2.length(), anwser.length());
                reponseTab.add(s2);
                }
            }
        return reponseTab;
        }

    public static ArrayList<Integer> getPourc(String response)
        {
        ArrayList<Integer> pourTab = new ArrayList<>();
        pourTab.add(50);
        pourTab.add(30);
        pourTab.add(20);
        return pourTab;
        }
    }
