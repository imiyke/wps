package com.example.miyke.wps;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.miyke.wps.Class.CustomList;

import java.util.ArrayList;

import com.example.miyke.wps.Class.Question;
import com.example.miyke.wps.Class.ToolKit;
import com.example.miyke.wps.Class.WebToolKit;


public class MainFlowActivity extends ActionBarActivity
        implements CustomNavigationDrawerFragment.NavigationDrawerCallbacks
    {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    public static CustomNavigationDrawerFragment mCustomNavigationDrawerFragment2;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    public static LinearLayout ll;
    static ArrayList<Question> list = null;
    public static SharedPreferences pref;
    public static Context           ctx;
    public static ArrayList<String> listCategorie;
    private Boolean needToReload = false;
    private ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        /** ================================== INIT SEQUENCE ====================================**/

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_flow);
        ll = (LinearLayout) findViewById(R.id.ll);
        if (needToReload)
            ll.removeAllViews();
        pb = ToolKit.setProgressBar(this, ll, pb);
        pref = getPreferences(Context.MODE_PRIVATE);
        ctx = this;
        mCustomNavigationDrawerFragment2 = (CustomNavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        mCustomNavigationDrawerFragment2.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        listCategorie = new ArrayList<>();
        listCategorie.add("Cinéma/TV");
        listCategorie.add("Informatique");
        listCategorie.add("Libre");
        listCategorie.add("Médical");
        listCategorie.add("Politique");

        /** ================================= CONNEXION SEQUENCE ================================**/

        list = WebToolKit.APIConnect("wpsquesttable", ll, ctx, pb);
        needToReload = true;

        }


    @Override
    public void onNavigationDrawerItemSelected(int position)
        {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
        }

    public void onSectionAttached(int number)
        {
/*        Intent intent = null;
        switch (number)
            {

            case 1:
               // intent=new Intent(this,MainFlowActivity.class);
                break;
            default:
                intent = new Intent(this, Categories.class);
                break;
            case 12:
                intent = new Intent(this, FavorisActivity.class);
                break;
            case 11:
                intent = new Intent(this, MyQuestionActivity.class);
                break;
            case 10:
                intent = new Intent(this, ReponduesActivity.class);
                break;
            case 30:
                intent = new Intent(this, FriendsActivity.class);
                break;
            case 40:
                intent = new Intent(this, AddQuestionActivity.class);

            }
        if (intent != null)
            startActivity(intent);*/

        }

    public void restoreActionBar()
        {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
        {
        if (!mCustomNavigationDrawerFragment2.isDrawerOpen())
            {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main_flow, menu);
            restoreActionBar();
            return true;
            }
        return super.onCreateOptionsMenu(menu);
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
        {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            {
            return true;
            }
        if (id == R.id.addButton)
            {
            Intent intent = new Intent(this, adminActivity.class);
            startActivity(intent);
            }

        return super.onOptionsItemSelected(item);
        }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment
        {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber)
            {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle              args     = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
            }

        public PlaceholderFragment()
            {
            }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
            {
            View rootView = inflater.inflate(R.layout.fragment_main_flow, container, false);
            return rootView;
            }

        @Override
        public void onAttach(Activity activity)
            {
            super.onAttach(activity);
            ((MainFlowActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
            }
        }

    }
