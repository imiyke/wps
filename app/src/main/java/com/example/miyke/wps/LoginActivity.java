package com.example.miyke.wps;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.miyke.wps.Class.ToolKit;
import com.example.miyke.wps.Class.User;
import com.example.miyke.wps.InternetClass.API;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class LoginActivity extends Activity
    {

    private Button logButton, createButton;
    private TextView loginError;
    private EditText userPass, userName;
    private       JSONObject jObj;
    public static JSONArray  JArray;
    public static boolean test  = false;
    private       boolean test2 = false;
    public static String             userlogged;
    public static User               user;
    private       LinearLayout       ll;
    public static ArrayList<Integer> threadList;
    public        ProgressBar        pb;
    public static Context loginCTX;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        /**_____________________________INIT SEQUENCE___________________________________________**/

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        logButton = (Button) findViewById(R.id.button_log);
        userName = (EditText) findViewById(R.id.TV_Login_username);
        userPass = (EditText) findViewById(R.id.TV_Login_Userpass);
        loginError = (TextView) findViewById(R.id.TV_Login_error);
        createButton = (Button) findViewById(R.id.Login_createButt);
        threadList = new ArrayList<>();
        userName.setText("Miyke"); /**TODO : enlever données hardcodées **/
        userPass.setText("mondmp");
        ll = (LinearLayout) findViewById(R.id.login_ll);
        pb = ToolKit.setProgressBar(this, ll, pb);
        pb.setVisibility(View.GONE);
        loginCTX=this;

        /**================================= ACTION SEQUENCE-LOG IN =============================**/

        logButton.setOnClickListener(new View.OnClickListener()
        {
        @Override
        public void onClick(View v)
            {
            pb.setVisibility(View.VISIBLE);
            DownloadDataLog();
            }
        });

        /** ================================= ACTION SEQUENCE-CREATE ACCOUNT ===================**/

        createButton.setOnClickListener(new View.OnClickListener()
        {
        @Override
        public void onClick(View v)
            {
            Intent intent = new Intent(LoginActivity.this, CreateAccount.class);
            startActivity(intent);
            }
        });
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
        {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
        {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
            {
            return true;
            }

        return super.onOptionsItemSelected(item);
        }

    public boolean CheckDataLog(String pseudo, String pwd) // compare DL data connexion to input
    // data
    {
       /* String [] temp=DownloadDataLog();
        if (temp[0]==pseudo && temp[1]==pwd)
            return true;
        else*/
    return false;

    }

    public void DownloadDataLog()// user data connexion
    {
    final String[] Array = new String[2];
    API MarmotAPI = new API(pb, ll, "wpsusertable/pseudo/" + userName.getText(), null, 2, new API
            .OnRequestListener()
    {
    @Override
    public void finish(String finalResult)
        {

        jObj = null;
        JArray = null;
        try
            {
            jObj = new JSONObject(finalResult);
            }
        catch (JSONException e1)
            {
            e1.printStackTrace();
            }

        try
            {
            Array[0] = jObj.getString("pseudo");
            Array[1] = jObj.getString("mdp");
            }
        catch (JSONException e)
            {
            System.out.println("ca marche pas psk:" + e.toString());
            e.printStackTrace();
            }
        Intent intent          = new Intent(LoginActivity.this, MainFlowActivity.class);
        String username        = Array[0];
        String username_config = userName.getText().toString();
        if (username.equals(username_config) && Array[1].equals(userPass.getText().toString()))
            {
            userlogged = username_config;
            try
                {

        /** =================== TAKE DATA TO DOWNLOAD USER FIELDS ==============================**/

                String myQuest = "";
                try
                    {
                    if (jObj.getString("myQuestions") != null)
                        myQuest = jObj.getString("myQuestions");
                    }
                catch (JSONException e)
                    {
                    e.printStackTrace();
                    }
                String myRep = "";
                try
                    {
                    if (jObj.getString("myReponses") != null)
                        myRep = jObj.getString("myReponses");
                    }
                catch (JSONException e)
                    {
                    e.printStackTrace();
                    }
                String favoris = "";
                try
                    {
                    if (jObj.getString("favoris") != null)
                        favoris = jObj.getString("favoris");
                    }

                catch (JSONException e)
                    {
                    e.printStackTrace();
                    }
                user = new User(username_config, jObj.getString("mdp"), myQuest,
                        myRep, jObj.getInt("nbReponse"), jObj.getInt("nbQuestion"), favoris, jObj
                        .getString("email"), jObj.getInt("id"), jObj.getString("status"));
                }
            catch (JSONException e)
                {
                e.printStackTrace();
                }
            startActivity(intent);
            } else
            {
            loginError.setText("Nom d'utilisateur ou mot de passse incorrect.\n Veuillez " +
                    "réessayer.");
            }

        }

    });
    }

    }
